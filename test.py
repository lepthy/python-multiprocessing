import hashlib

def compute_digest(filename):
    digest = hashlib.sha512()
    with open(filename, 'rb') as fd:
        while True:
            chunk = fd.read(8192)
            if not chunk: break
            digest.update(chunk)
    return digest.digest()


##############
import multiprocessing
import os, sys

topdir = sys.argv[1]
poolSize = int(sys.argv[2])

p = multiprocessing.Pool(poolSize)
digest_map = {}
for path, dirs, files in os.walk(topdir):
    for name in files:
        fullname = os.path.join(path, name)
        digest_map[fullname] = p.apply_async(compute_digest, (fullname,))

for filename, result in digest_map.items():
    digest_map[filename] = result.get()

print(digest_map)
